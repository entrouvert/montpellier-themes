VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="montpellier-themes"

prefix = /usr

all:
	(cd wcs/montpellier2 && msgfmt -o fr.mo fr.po)
	(cd wcs/montpellier-commune && msgfmt -o fr.mo fr.po)
	(cd wcs/montpellier-ville && msgfmt -o fr.mo fr.po)

clean:
	rm -f wcs/montpellier2/fr.mo
	rm -f wcs/montpellier-commune/fr.mo
	rm -f wcs/montpellier-ville/fr.mo
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/montpellier-theme/templates
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/montpellier2
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/montpellier-commune
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/montpellier-ville
	cp -r templates/* $(DESTDIR)$(prefix)/share/publik/themes/montpellier-theme/templates/
	cp -ar wcs/montpellier2/* $(DESTDIR)$(prefix)/share/wcs/themes/montpellier2/
	cp -ar wcs/montpellier-commune/* $(DESTDIR)$(prefix)/share/wcs/themes/montpellier-commune/
	cp -ar wcs/montpellier-ville/* $(DESTDIR)$(prefix)/share/wcs/themes/montpellier-ville/

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

