$(function() {
  $('.dataview').before('<a href="#" id="disclose-dataview">Afficher le détail de la demande</a>');
  $('#disclose-dataview').click(
    function() {
      $(this).hide();
      $('.dataview').show();
      return false;
    });
});
