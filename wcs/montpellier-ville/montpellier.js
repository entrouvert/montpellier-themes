$(function() {
  $('#home-page-intro').after(
     '<form id="tracking-form" action="/code/load"> \
     <table><tr> \
     <td><h3>Code&nbsp;de&nbsp;suivi</h3></td> \
     <td><input required size="12" name="code" placeholder="ex: RPQDFVCD"/></td> \
     <td><input type="submit" value="Valider"/></td> \
     <td class="p"><p>Un code de suivi peut &ecirc;tre associ&eacute; &agrave; vos demandes, \
        il vous facilite les &eacute;changes avec les services.  Pour retrouver \
        une demande disposant d\'un code de suivi, indiquez ce dernier ci-contre. \
     </p></td> \
     </tr></table></form>');
});
